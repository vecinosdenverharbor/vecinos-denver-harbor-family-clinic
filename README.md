High-quality affordable family health center to medically underserved communities in Denver Harbor, Houston. We speak spanish.

Our services:
Health care for adults and children
Medical, dental & counseling
Management of diabetes and high blood pressure
Speciality care
Educational events.

Address: 424 Hahlo Street, Houston, TX 77020 || Phone: 713-674-3326
